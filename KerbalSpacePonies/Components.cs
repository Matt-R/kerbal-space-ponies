using System;
using UnityEngine;

namespace KerbalSpacePonies
{
    public class IvaModule: MonoBehaviour
    {
        public void Start()
        {
            var kerbal = GetComponent<Kerbal>();
            new Ponifier(kerbal, kerbal.protoCrewMember).Ponify();
            gameObject.AddComponent<VisibilityChecker>();
        }
    }
    
    public class EvaModule: PartModule
    {
        private bool isInitialised = false;
        
        public override void OnStart(StartState state)
        {
            if (!isInitialised)
            {
                isInitialised = true;
                new Ponifier(part, part.protoModuleCrew[0]).Ponify();
                gameObject.AddComponent<VisibilityChecker>();
            }
        }
    }

    public class VisibilityChecker: MonoBehaviour
    {
        private Renderer head, eyes, mane, horn, kerbalHead;

        public void Start()
        {
            foreach (var smr in GetComponentsInChildren<SkinnedMeshRenderer>(true))
            {
                switch (smr.name)
                {
                    case "headMesh01":
                    case "mesh_female_kerbalAstronaut01_kerbalGirl_mesh_polySurface51":
                    case "headMesh":
                        kerbalHead = smr;
                        break;
                    case "ponyHead":
                        head = smr;
                        break;
                    case "ponyEyes":
                        eyes = smr;
                        break;
                    case "mane":
                        mane = smr;
                        break;
                    case "horn":
                        horn = smr;
                        break;
                }
            }
        }

        public void Update()
        {
            // Hide all head meshes when kerbal head is hidden,
            // like in IVA first-person view.
            if (!kerbalHead) return;
            if (head) head.enabled = kerbalHead.enabled;
            if (eyes) eyes.enabled = kerbalHead.enabled;
            if (mane) mane.enabled = kerbalHead.enabled;
            if (horn) horn.enabled = kerbalHead.enabled;
        }
    }
}
