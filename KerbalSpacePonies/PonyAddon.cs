using System;
using UnityEngine;

namespace KerbalSpacePonies
{
    [KSPAddon(KSPAddon.Startup.Instantly, true)]
    public class PonyAddon: MonoBehaviour
    {
        private bool isInitialized = false;
        private Game game;

        public void Start()
        {
            PonyConfigs.instance = new PonyConfigs();
            GameEvents.onKerbalAdded.Add(onKerbalAdd);
            GameEvents.onGameStateCreated.Add(setGame);
        }

        public void LateUpdate()
        {
            if (!isInitialized && PartLoader.Instance.IsReady())
            {
                foreach (Kerbal kerbal in Resources.FindObjectsOfTypeAll<Kerbal>())
                {
                    kerbal.gameObject.AddComponent<IvaModule>();
                }
                PartLoader.getPartInfoByName("kerbalEVAfemale").partPrefab.gameObject.AddComponent<EvaModule>();

                PonyConfigs.instance.Load();

                isInitialized = true;
            }
        }

        private void onKerbalAdd(ProtoCrewMember crew)
        {
            if (crew.type != ProtoCrewMember.KerbalType.Applicant) return;
            if (crew.name.GetHashCode() % 3 != 0) return;

            foreach(var name in PonyConfigs.instance.PonyNames)
            {
                if (!game.CrewRoster.Exists(name))
                {
                    crew.name = name;
                    PonifyCrewMember(crew);
                    break;
                }
            }
        }

        private void PonifyCrewMember(ProtoCrewMember crew)
        {
            var pony = PonyConfigs.instance.GetPony(crew.name);
            if (pony == null) return;

            crew.gender = pony.gender;
            crew.courage = pony.courage;
            crew.stupidity = pony.stupidity;
            crew.isBadass = true;

            KerbalRoster.SetExperienceTrait(crew, pony.trait);
        }

        private void setGame(Game g) {
            game = g;
            foreach(var crew in game.CrewRoster.Crew)
            {
                PonifyCrewMember(crew);
            }
        }

    }
}

