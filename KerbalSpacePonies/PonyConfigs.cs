using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace KerbalSpacePonies
{
    public class PonyPart
    {
        public Mesh mesh;
        public Material material;

        public PonyPart(Mesh mesh, Material material)
        {
            this.mesh = mesh;
            this.material = material;
        }
    }

    public class Pony
    {
        public string name;
        public PonyPart head, mane, eyes, horn;
        public string trait = "Pilot";
        public float courage = 0.5f, stupidity = 0.5f;
        public ProtoCrewMember.Gender gender = ProtoCrewMember.Gender.Female;
    }

    public class PonyConfigs
    {
        public static PonyConfigs instance = null;
        private Dictionary<string, Pony> ponies;
        private Matrix4x4 bindpose;

        public IEnumerable<string> PonyNames { get { return ponies.Keys; } }

        public PonyConfigs()
        {
            ponies = new Dictionary<string, Pony>();
        }

        private Pony CreatePony(string name)
        {
            var pony = new Pony();
            pony.name = name;
            return ponies[name] = pony;
        }

        private static string DIR = "GameData/KerbalSpacePonies/";
        private static string TEX_DIR = DIR + "Textures/";
        private static string MODELS_DIR = DIR + "MODELS/";

        public void Load()
        {
            Pony pony;

            bindpose = GetBindPose();
            var ponyHead = LoadMesh("Head");
            var eyes = LoadMesh("Eyes");
            var ponyHorn = LoadMesh("Horn");
            var rainbowMane = LoadMesh("RainbowMane");

            var bodyTexture = LoadTexture(TEX_DIR + "body.png");
            var hornTexture = LoadTexture(TEX_DIR + "horn.png");

            {
                pony = CreatePony("Twilight Sparkle");
                var twilightCoatColor = new Color(199/255f, 157/255f, 215/255f);
                var twilightManeColor = new Color (54/255f, 59/255f, 116/255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, twilightCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "twilight_eyes.png"));
                pony.mane = new PonyPart(LoadMesh("TwilightMane"), CreateMaterial(TEX_DIR + "twilight_mane.png"));
                pony.horn = new PonyPart(ponyHorn, CreateMaterial(hornTexture, twilightCoatColor));
                pony.trait = "Scientist";
                pony.courage = 0.613453f;
                pony.stupidity = 0.0f;
            }

            {
                pony = CreatePony("Rainbow Dash");
                var dashCoatColor = new Color(135/255f, 209/255f, 242/255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, dashCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "rainbow_eyes.png"));
                pony.mane = new PonyPart(rainbowMane, CreateMaterial(TEX_DIR + "rainbow_mane.png"));
                pony.trait = "Pilot";
                pony.courage = 1.0f;
                pony.stupidity = 0.6f;
            }

            {
                pony = CreatePony("Pinkie Pie");
                var pinkieCoatColor = new Color(248/255f, 185/255f, 206/255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, pinkieCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "pinkie_eyes.png"));
                pony.mane = new PonyPart(LoadMesh("PinkieMane"), CreateMaterial(TEX_DIR + "pinkie_mane.png"));
                pony.trait = "Engineer";
                pony.courage = 0.83435f;
                pony.stupidity = 0.1276585f;
            }

            {
                pony = CreatePony("Applejack");
                pony.head = new PonyPart(ponyHead, CreateMaterial(TEX_DIR + "applejack_body.png"));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "applejack_eyes.png"));
                pony.mane = new PonyPart(LoadMesh("ApplejackMane"), CreateMaterial(TEX_DIR + "applejack_mane.png"));
                pony.trait = "Engineer";
                pony.courage = 0.8859576f;
                pony.stupidity = 0.1996755f;
            }

            {
                pony = CreatePony("Lyra Heartstrings");
                var lyraCoatColor = new Color(148/255f, 1f, 220/255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, lyraCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "lyra_eyes.png"));
                pony.mane = new PonyPart(LoadMesh("LyraMane"), CreateMaterial(TEX_DIR + "lyra_mane.png"));
                pony.horn = new PonyPart(ponyHorn, CreateMaterial(hornTexture, lyraCoatColor));
                pony.trait = "Scientist";
                pony.courage = 0.6111993f;
                pony.stupidity = 0.4804350f;
            }
            
            {
                pony = CreatePony("Bon Bon");
                var bonbonCoatColor = new Color(245/255f, 247/255f, 217/255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, bonbonCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "bonbon_eyes.png"));
                pony.mane = new PonyPart(LoadMesh("BonbonMane"), CreateMaterial(TEX_DIR + "bonbon_mane.png"));
                pony.trait = "Scientist";
                pony.courage = 0.6295045f;
                pony.stupidity = 0.1628060f;
            }

            {
                pony = CreatePony("Trixie Lulamoon");
                var trixieCoatColor = new Color(85/255f, 172/255f, 243/255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, trixieCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "trixie_eyes.png"));
                pony.mane = new PonyPart(LoadMesh("TrixieMane"), CreateMaterial(TEX_DIR + "trixie_mane.png"));
                pony.horn = new PonyPart(ponyHorn, CreateMaterial(hornTexture, trixieCoatColor));
                pony.trait = "Engineer";
                pony.courage = 0.9832337f;
                pony.stupidity = 0.6583056f;
            }

            {
                pony = CreatePony("Derpy Hooves");
                var derpyCoatColor = new Color(193/255f, 197/255f, 211/255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, derpyCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "derpy_eyes.png"));
                pony.mane = new PonyPart(rainbowMane, CreateMaterial(new Color(239/255f, 244/255f, 180/255f)));
                pony.trait = "Pilot";
                pony.courage = 0.728016f;
                pony.stupidity = 0.7630379f;
            }
        }

        private static Matrix4x4 GetBindPose()
        {
            var eva = PartLoader.getPartInfoByName("kerbalEVAfemale").partPrefab.gameObject;
            foreach (SkinnedMeshRenderer smr in eva.GetComponentsInChildren<SkinnedMeshRenderer>(true))
            {
                switch (smr.name)
                {
                    case "headMesh01":
                    case "mesh_female_kerbalAstronaut01_kerbalGirl_mesh_polySurface51":
                    case "headMesh":
                        int i = 0;
                        foreach(var bone in smr.bones)
                        {
                            if (bone.name == "bn_upperJaw01")
                            {
                                return smr.sharedMesh.bindposes[i];
                            }
                            i++;
                        }
                        break;
                }
            }
            System.Diagnostics.Debug.Assert(false, "GetBindPose failed");
            return new Matrix4x4();
        }

        private Mesh CreateMesh(Vector3[] vertices, Vector2[] texcoords, int[] triangles)
        {
            var mesh = new Mesh();
            mesh.vertices = vertices;
            mesh.uv = texcoords;
            BoneWeight[] weights = new BoneWeight[mesh.vertices.Length];
            for(int i = 0; i < weights.Length; i++)
            {
                weights[i].boneIndex0 = 0;
                weights[i].weight0 = 1;
            }
            mesh.boneWeights = weights;
            mesh.bindposes = new Matrix4x4[] {bindpose};
            mesh.triangles = triangles;
            mesh.RecalculateNormals();

            return mesh;
        }

        private Vector3[] LoadVector3Array(string path)
        {
            using(FileStream fs = File.OpenRead(path))
            using(BinaryReader r = new BinaryReader(fs))
            {
                var a = new Vector3[r.ReadUInt32()];
                for(int i = 0; i < a.Length; i++)
                {
                    a[i].x = r.ReadSingle();
                    a[i].y = r.ReadSingle();
                    a[i].z = r.ReadSingle();
                }
                return a;
            }
        }

        private Vector2[] LoadVector2Array(string path)
        {
            using(FileStream fs = File.OpenRead(path))
            using(BinaryReader r = new BinaryReader(fs))
            {
                var a = new Vector2[r.ReadUInt32()];
                for(int i = 0; i < a.Length; i++)
                {
                    a[i].x = r.ReadSingle();
                    a[i].y = r.ReadSingle();
                }
                return a;
            }
        }

        private int[] LoadIntArray(string path)
        {
            using(FileStream fs = File.OpenRead(path))
            using(BinaryReader r = new BinaryReader(fs))
            {
                var a = new int[r.ReadUInt32()];
                for(int i = 0; i < a.Length; i++)
                {
                    a[i] = r.ReadInt32();
                }
                return a;
            }
        }

        private Mesh LoadMesh(string name)
        {
            string basepath = MODELS_DIR + name;
            return CreateMesh(
                LoadVector3Array(basepath + ".vtx"),
                LoadVector2Array(basepath + ".tex"),
                LoadIntArray(basepath + ".idx")
            );
        }

        private Material CreateMaterial(Color color)
        {
            var mat = new Material(Shader.Find("Diffuse"));
            mat.color = color;
            return mat;
        }

        private Texture2D LoadTexture(string path)
        {
            var tex = new Texture2D(1, 1);
            try {
                tex.LoadImage(File.ReadAllBytes(path));
            } catch (Exception e) {
                Debug.LogException(e);
            }
            return tex;
        }

        private Material CreateMaterial(string path)
        {
            var tex = LoadTexture(path);
            return CreateMaterial(tex);
        }

        private Material CreateMaterial(Texture2D texture)
        {
            return CreateMaterial(texture, Color.white);
        }

        private Material CreateMaterial(Texture2D texture, Color color)
        {
            var mat = new Material(Shader.Find("Diffuse"));
            mat.mainTexture = texture;
            mat.color = color;
            return mat;
        }

        public Pony GetPony(string name)
        {
            Pony pony = null;
            ponies.TryGetValue(name, out pony);
            return pony;
        }
    }
}

